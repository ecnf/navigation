### JNavigation 的使用


```bash
cd use

npm i joking-navigation
```

```bash
加载个人及系统配置文件用了 508 毫秒。
Administrator@DESKTOP-F6STM39  G:\..\navigation  cd use
Administrator@DESKTOP-F6STM39  G:\..\navigation\use  npm i joking-navigation
npm WARN @vue/cli-service@4.5.10 requires a peer of @vue/compiler-sfc@^3.0.0-beta.14 but none is installed. You must install peer dependencies yourself.
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@2.3.1 (node_modules\fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@2.3.1: wanted {"os":"darwin","arch":"any"} (current: {"os":"win32","arch":"x64"})
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@1.2.13 (node_modules\watchpack-chokidar2\node_modules\fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.13: wanted {"os":"darwin","arch":"any"} (current: {"os":"win32","arch":"x64"})
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@1.2.13 (node_modules\webpack-dev-server\node_modules\fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.13: wanted {"os":"darwin","arch":"any"} (current: {"os":"win32","arch":"x64"})

+ joking-navigation@0.4.0
added 1 package from 1 contributor and audited 1360 packages in 9.914s
found 0 vulnerabilities
```

## `main.js`

```js
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import 'joking-navigation'
import 'joking-navigation/dist/joking-navigation.css'

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')

```


## `Home.vue`

```vue
<template>
  <div class="home">
      <navigation mapAK="VkUxZB5MuTnyqTESgLBoakZASjez44wc" :positionNow="position" :name="endName"></navigation>
  </div>
</template>
<script>
export default {
  name: 'Home',
  data () {
      return {
          position: [113.238461,27.999319], // 终点的经纬度
          endName: '可自定义终点名称，百度导航同用这个终点名', // 终点名称
      }
    }
}
</script>

```