import Vue from 'vue'
import App from './App.vue'
import router from './router'
import 'joking-navigation'
import 'joking-navigation/navigation/index.css'

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
