module.exports = {
    devServer: {
        https:false
    },
    publicPath: '/use',
    lintOnSave: false,
}
