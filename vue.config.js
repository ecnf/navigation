module.exports = {
  devServer: {
    https: false
  },
  publicPath: '/navigation',
  lintOnSave: false,
  outputDir: 'navigation'
}
