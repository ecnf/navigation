<p align="center">
  <a href="https://www.npmjs.com/package/joking-navigation" target="_blank">
    <img width="160" src="https://cdn.jsdelivr.net/gh/nidhoggdjoking/CDN@v2.3-bate/img/Map.png" alt="logo"/>
  </a>
</p>

<h2 align="center">JNavigation</h2>
<p align="center">
  <a href="https://github.com/vuejs/vue" target="_blank">
    <img src="https://img.shields.io/badge/vue-2.6.11-green?logo=Vue.js">
  </a>
  <a href="https://www.npmjs.com/package/joking-navigation" target="_blank">
    <img src="https://img.shields.io/badge/NPM-rely--on-red?logo=npm&style=flat-square"/>
  </a>
  <a href="http://lbsyun.baidu.com/" target="_blank">
    <img src="https://img.shields.io/badge/baidu--map-2.0-9cf?logo=Baidu"/>
  </a>
</p>


## 上手使用:

### 安装

```sh
npm i joking-navigation
```

`main.js` 引用

```JavaScript
import 'joking-navigation';
import 'joking-navigation/navigation/index.css'
```

### `.vue` 文件文件内直接使用 :

```html
<navigation></navigation>
```

`0.4.0 以后伸缩框底部内容改为插槽可自定义不在局限于列表`

### Props :

| 参数	| 说明 | 类型 | 默认值 |
| :-------- | :--------| :-------- | :-------- |
| mapAK	| 百度地图AK(必填) | String |`-`|
| version	| 百度地图API版本 | String |`3.0`|
| name	| 终点名称| Array |`-`|
| position	| 终点的经纬度 | String |`[116.403963, 39.915122], // 默认天安门`|


`使用效果如下图:`

<img style="width:250px;float:left" src="https://nidhoggdjoking.gitee.io/storage/project/JNavigation/case1.png"/>



📌 [不熟悉百度地图API的建议先熟悉他们的官方文档](http://lbsyun.baidu.com/cms/jsapi/reference/jsapi_reference.html)


### 插件日志

```
//关于状态码
//BMAP_STATUS_SUCCESS        检索成功。对应数值“0”。
//BMAP_STATUS_CITY_LIST        城市列表。对应数值“1”。
//BMAP_STATUS_UNKNOWN_LOCATION        位置结果未知。对应数值“2”。
//BMAP_STATUS_UNKNOWN_ROUTE        导航结果未知。对应数值“3”。
//BMAP_STATUS_INVALID_KEY        非法密钥。对应数值“4”。
//BMAP_STATUS_INVALID_REQUEST        非法请求。对应数值“5”。
//BMAP_STATUS_PERMISSION_DENIED        没有权限。对应数值“6”。(自 1.1 新增)
//BMAP_STATUS_SERVICE_UNAVAILABLE        服务不可用。对应数值“7”。(自 1.1 新增)
//BMAP_STATUS_TIMEOUT        超时。对应数值“8”。(自 1.1 新增)
```

```JavaScript

// 时间计算格式要改 1小时30分钟  => 1.5小时

plan.getDuration(true); => plan.getDuration(false);

// 返回的是秒

```


### 不考虑跨城交通方式策略

```
IntercityPolicy
此常量表示跨城公交换乘策略。
常量	描述
BMAP_INTERCITY_POLICY_LEAST_TIME	时间短
BMAP_INTERCITY_POLICY_EARLY_START	出发早
BMAP_INTERCITY_POLICY_CHEAP_PRICE	价格低
```

```
TransitPolicy
此常量表示市内公交方案换乘策略。
常量	描述
BMAP_TRANSIT_POLICY_RECOMMEND	推荐方案
BMAP_TRANSIT_POLICY_LEAST_TIME	最少时间
BMAP_TRANSIT_POLICY_LEAST_TRANSFER	最少换乘
BMAP_TRANSIT_POLICY_LEAST_WALKING	最少步行
BMAP_TRANSIT_POLICY_AVOID_SUBWAYS	不乘地铁
BMAP_TRANSIT_POLICY_FIRST_SUBWAYS	优先地铁
```


### 打赏

<div style="width:200px">
<img style="width:250px;display: block;" src="https://nidhoggdjoking.gitee.io/storage/common/img/wxpay.png"/>
</div>

<div style="width:200px">
<img  style="width:250px;display: block;" src="https://nidhoggdjoking.gitee.io/storage/common/img/alipay.jpg"/>
</div>
