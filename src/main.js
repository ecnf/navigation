import Vue from 'vue'
import App from './App.vue'

// 引用我们的自定义组件
import '@/components'

Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
