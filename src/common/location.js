// 多线程异步
export function getLocation (that) {
  var geolocation = new BMap.Geolocation()
  geolocation.getCurrentPosition(function (r) {
    if (this.getStatus() == BMAP_STATUS_SUCCESS) {
      that.position.lng = r.point.lng
      that.position.lat = r.point.lat
      that.position.city = r.address.city
      that.position.province = r.address.province
      // console.log(r)
      return true
    }
  })
}
// 单线程异步
export function getLocationSync (that) {
  return new Promise(function (resolve, reject) {
    var geolocation = new BMap.Geolocation()
    geolocation.getCurrentPosition(function (r) {
      if (this.getStatus() == BMAP_STATUS_SUCCESS) {
        that.position.lng = r.point.lng
        that.position.lat = r.point.lat
        that.position.city = r.address.city
        that.position.province = r.address.province
        resolve(r)
        return true
      }
    })
  })
}
