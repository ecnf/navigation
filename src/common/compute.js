// 公交计算
export function transit (map, that, show = false) {
  // 公交策略：最佳推荐方案，最少时间，最少换乘,最少步行,不乘地铁,优先地铁 (V2和V3 策略字符完全不一样需查看文档)
  // todo  暂时不搞策略
  const routePolicy = [BMAP_TRANSIT_POLICY_RECOMMEND, BMAP_TRANSIT_POLICY_LEAST_TIME, BMAP_TRANSIT_POLICY_LEAST_TRANSFER, BMAP_TRANSIT_POLICY_LEAST_WALKING, BMAP_TRANSIT_POLICY_AVOID_SUBWAYS, BMAP_TRANSIT_POLICY_FIRST_SUBWAYS]
  var searchComplete = function (results) {
    if (transit.getStatus() != BMAP_STATUS_SUCCESS) {
      return
    }
    var plan = results.getPlan(0)
    that.transitData.time = plan.getDuration(true)
    that.transitData.distance = plan.getDistance(true)
    // if (that.isFoot != 0) transit.clearResults();
  }
  var transit
  if (show) {
    transit = new BMap.TransitRoute(map, {
      renderOptions: {
        map: map,
        autoViewport: true
      },
      onSearchComplete: searchComplete,
      onPolylinesSet: function () {
        // console.log(transit)
        // if (that.isFoot != 0) transit.clearResults();
      }
    })
  } else {
    transit = new BMap.TransitRoute(map, {
      onSearchComplete: searchComplete
    })
  }

  var start = new BMap.Point(that.position.lng, that.position.lat)
  var end = new BMap.Point(that.endSpot[0], that.endSpot[1])
  transit.search(start, end)
  getIcon(transit)
  return transit
}
// 骑行
export function riding (map, that, show = false) {
  var searchComplete = function (results) {
    if (riding.getStatus() != BMAP_STATUS_SUCCESS) {
      return
    }
    var plan = results.getPlan(0)
    that.ridingData.time = plan.getDuration(true)
    that.ridingData.distance = plan.getDistance(true)
    // if (that.isFoot != 1) riding.clearResults();
  }
  var riding
  if (show) {
    riding = new BMap.RidingRoute(map, {
      renderOptions: {
        map: map,
        autoViewport: true
      },
      onSearchComplete: searchComplete,
      onPolylinesSet: function () {
        // if (that.isFoot != 1) riding.clearResults();
      }
    })
  } else {
    riding = new BMap.RidingRoute(map, {
      onSearchComplete: searchComplete
    })
  }

  var start = new BMap.Point(that.position.lng, that.position.lat)
  var end = new BMap.Point(that.endSpot[0], that.endSpot[1])
  riding.search(start, end)
  getIcon(riding)
  return riding
}

// 驾车计算
export function drive (map, that, show = false) {
  // 驾车策略：默认，优先高速，避开高速,避开拥堵 (V2和V3 策略字符完全不一样需查看文档)
  // todo  暂时不搞策略
  const routePolicy = [BMAP_DRIVING_POLICY_DEFAULT, BMAP_DRIVING_POLICY_FIRST_HIGHWAYS, BMAP_DRIVING_POLICY_AVOID_HIGHWAYS, BMAP_DRIVING_POLICY_AVOID_CONGESTION]
  var searchComplete = function (results) {
    if (driving.getStatus() != BMAP_STATUS_SUCCESS) {
      return
    }
    var plan = results.getPlan(0)
    that.driveData.time = plan.getDuration(true)
    that.driveData.distance = plan.getDistance(true)
    // if (that.isFoot != 2) driving.clearResults();
  }
  var driving

  if (show) {
    driving = new BMap.DrivingRoute(map, {
      renderOptions: {
        map: map,
        autoViewport: true
      },
      onSearchComplete: searchComplete,
      onPolylinesSet: function () {
        // if (that.isFoot != 2) driving.clearResults();
      }
    })
  } else {
    driving = new BMap.DrivingRoute(map, {
      onSearchComplete: searchComplete
    })
  }
  // V3 版本语法兼容V2 但不在支持接受String类型的位置信息
  // driving.search("长沙麓谷企业广场", "长沙五一广场");
  var start = new BMap.Point(that.position.lng, that.position.lat)
  var end = new BMap.Point(that.endSpot[0], that.endSpot[1])
  driving.search(start, end)
  getIcon(driving)
  return driving
}
// 步行计算
export function walk (map, that, show = false) {
  var searchComplete = function (results) {
    if (walking.getStatus() != BMAP_STATUS_SUCCESS) {
      return
    }
    var plan = results.getPlan(0)
    that.walkData.time = plan.getDuration(true)
    that.walkData.distance = plan.getDistance(true)
    // if (that.isFoot != 3) walking.clearResults();
  }
  var walking
  if (show) {
    walking = new BMap.WalkingRoute(map, {
      renderOptions: {
        map: map,
        autoViewport: true
      },
      onSearchComplete: searchComplete,
      onPolylinesSet: function () {
        // if (that.isFoot != 3) walking.clearResults();
      }
    })
  } else {
    walking = new BMap.WalkingRoute(map, {
      onSearchComplete: searchComplete
    })
  }

  var start = new BMap.Point(that.position.lng, that.position.lat)
  var end = new BMap.Point(that.endSpot[0], that.endSpot[1])
  walking.search(start, end)
  getIcon(walking)
  return walking
}

function getIcon (obj) {
  var myIcon1 = new BMap.Icon(require('@/assets/icon/StartPoint.png'), new BMap.Size(50, 55))
  var myIcon2 = new BMap.Icon(require('@/assets/icon/EndPoint.png'), new BMap.Size(50, 55))
  // 图标偏差值
  myIcon1.setAnchor(new BMap.Size(24, 45))
  myIcon2.setAnchor(new BMap.Size(24, 45))
  obj.setMarkersSetCallback(function (result) {
    result[0].marker.setIcon(myIcon1)
    result[1].marker.setIcon(myIcon2)
  })
}
