import Vue from 'vue'
import navigation from './navigation.vue'

const Components = {
  navigation
}

Object.keys(Components).forEach(name => {
  Vue.component(name, Components[name])
})

export default Components
