import { shallowMount } from '@vue/test-utils'
import navigation from '@/components/navigation.vue'

describe('HelloWorld.vue', () => {
  it('renders props.msg when passed', () => {
    const msg = 'new message'
    const wrapper = shallowMount(navigation, {
      propsData: { 
        
      }
    })
    expect(wrapper.text()).toMatch(msg)
  })
})
